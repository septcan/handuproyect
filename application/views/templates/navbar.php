<style type="text/css">
     #sidebarnav>li>a {
    /*width: 200px !important; */
    padding: 0px 12px 0px 15px !important;
}
  .sidebar-nav>ul>li>a i {
    width: 35px !important;
    font-size: 30px !important;
    display: inline-block;
    vertical-align: middle;
    color: black !important;
  }
  .vd_red{
    color: red;
  }
  .vd_green{
    color: green;
  }
  .skin-default .topbar {
    background: #fdc705;
  }
  .sidebar-nav ul li a.active, .sidebar-nav ul li a.active i, .sidebar-nav ul li a:hover, .sidebar-nav ul li a:hover i {
    color: #fdc705;
}
</style>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Cargando</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header" style="background-color: white !important;">
                    <a class="navbar-brand" href="<?php echo base_url();?>Inicio">
                        <!-- Logo icon -->
                        <b>
                            <img src="<?php echo base_url();?>images/handu.jpg" alt="homepage" class="dark-logo" />
                            <!-- Light Logo icon -->
                            <img style="width:65px" src="<?php echo base_url();?>images/handu.jpg" alt="homepage" class="light-logo" />
                        </b>
                        <!--End Logo icon -->
                        <span>
                         <!-- dark Logo text -->
                         <img src="<?php echo base_url();?>images/handu2p.jpg" alt="homepage" class="dark-logo" />
                         <!-- Light Logo text -->    
                         <img style="width:135px" src="<?php echo base_url();?>images/handu2p.jpg" class="light-logo" alt="homepage" />
                       </span> 
                   </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    
                    <ul class="navbar-nav mr-auto">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler d-block d-md-none waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <li class="nav-item"> <a class="nav-link sidebartoggler d-none d-lg-block d-md-block waves-effect waves-dark" href="javascript:void(0)"><i class="icon-menu"></i></a> </li>

                        <li class="nav-item dropdown u-pro">
                            <a class="nav-link profile-pic">   
                            </a>
                        </li>
                    </ul>
                    
                    <ul class="navbar-nav my-lg-0">
                        <li class="nav-item"> <a class="nav-link" href="<?php echo base_url();?>Login/logout"><i class="fas fa-sign-out-alt"></i> Salir</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li> 
                            <a class="waves-effect waves-dark" href="#" aria-expanded="false"><i class="fas fa-cogs"></i> <span class="hide-menu"> Configuraciones</span></a>
                        </li>
                        <li> 
                            <a class="waves-effect waves-dark" href="#" aria-expanded="false"><i class="fas fa-box-open"></i> <span class="hide-menu"> Operaciones</span></a>
                        </li>
                        <li> 
                            <a class="waves-effect waves-dark" href="#" aria-expanded="false"><i class="fas fa-users"></i> <span class="hide-menu"> Personal</span></a>
                        </li>
                        <li> 
                            <a class="waves-effect waves-dark" href="#" aria-expanded="false"><i class="fas fa-truck-moving"></i> <span class="hide-menu"> Proveedores</span></a>
                        </li>
                        <li> 
                            <a class="waves-effect waves-dark" href="#" aria-expanded="false"><i class="fas fa-folder"></i> <span class="hide-menu"> Soliciudes</span></a>
                        </li>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->

        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
