<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>images/handu.jpg">
    <title>HANDU</title>
    <!-- Custom CSS -->
    <link href="<?php echo base_url();?>assets/node_modules/toast-master/css/jquery.toast.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/node_modules/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
    <!-- page css -->
    <link href="<?php echo base_url();?>assets/node_modules/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/dist/css/pages/user-card.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/dist/css/style.min.css" rel="stylesheet">
</head>
<style type="text/css">
    .skin-default .topbar {
        background: orange;
    }
</style>
<input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
<body class="fixed-layout skin-default">