<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>HANDU</title>
    <!-- Favicon-->
    <link rel="icon" href="<?php echo base_url(); ?>images/handu.jpg" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url();?>plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?php echo base_url();?>plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?php echo base_url();?>plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?php echo base_url();?>css/style.css" rel="stylesheet">
    <script>
        var base_url="<?php echo base_url(); ?>";
    </script>
</head>

<body class="login-page">
    <div class="sesion_texto" style="width: 400px;
    height: 400px;
    position: absolute;
    top: 50%;
    left: 50%;
    margin-left: -200px;
    margin-top: -200px;
    z-index: 1;
    display: none;" align="center">
            <!-- style="background: #ffffffad; border-radius: 20px;" 
            
            -->   
        <img style="" src="<?php echo base_url();?>images/handu.jpg" width="50%" class="m-t-90"  />
        
        <h2 style="color: black;">Iniciando Sesión</h2>
        <h3 style="color: black;">Conectado a la base de datos</h3>
    </div>

    <div class="cx3"></div>
    <div class="cx2"></div>
    <div class="cx1"></div>
    
    <div class="login-box" align="center">
        <div class="card login_texto">
            <div class="body">
                <form id="sign_in" method="POST">
                    <div class="row align-center">
                        <div class="col-md-6">
                           <img src="<?php echo base_url();?>images/handu.jpg" width="70%" />
                           <img src="<?php echo base_url();?>images/handu4.jpg" width="70%" />
                        </div>
                        <div class="col-md-6" style="border-left: 1px solid #FFEB3B;">
                            <i class="material-icons font-40 m-t-20 col-yellow">lock</i>
                            <h2>Accede a tu cuenta</h2>
                            <h5>Estamos contentos de que regreses</h5>
                            <br>
                            <div class="form-group input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">person</i>
                                </span>
                                <div class="form-line">
                                    <input type="text" class="form-control" name="usuario" placeholder="Nombre de Usuario" required autofocus>
                                </div>
                            </div>
                            <div class="form-group input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">lock</i>
                                </span>
                                <div class="form-line">
                                    <input type="password" class="form-control" name="password" placeholder="Contraseña" required>
                                </div>
                            </div>
                            <div class="row"> 
                                <div class="col-xs-12">
                                    <button class="btn btn-block bg-yellow waves-effect" style="color: black !important;" type="submit">INICIAR SESIÓN</button>
                                </div>
                            </div>
                            <br>
                            <!--
                            <p class="align-center"><span style="color:#7d939a;">¿Olvidaste tu contraseña?</span> 
                                <a id="resetPass" ><span class="textEleonor">Restablécela aquí</span></a></p>
                            <p class="align-center"><a href="/register/medic" id="registerLink" class="rastreable" ><span style="color:#ffe821;">¿Aún no tienes una cuenta? </span><span class="textEleonor">Regístrate</span> </a></p>
                            -->
                        </div>
                    </div>
                </form>
            </div>
        <div>    
    </div>
    </div>
    <div class="text-center" id="loader">
        <div class="lds-ripple"><div></div><div></div></div>
    </div>
    <!--
    <div class="alert bg-light-green" id="success">
        <i class="material-icons ">done</i> <strong>Acceso Correcto!</strong> Será redirigido al sistema 
    </div>
    -->
    <div class="alert bg-pink" id="error">
        <i class="material-icons ">error</i> <strong>Error!</strong> El nombre de usuario y/o contraseña son incorrectos 
    </div>
    
    <!-- Jquery Core Js -->
    <script src="<?php echo base_url();?>plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url();?>plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url();?>plugins/node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="<?php echo base_url();?>plugins/jquery-validation/jquery.validate.js"></script>

    <!-- Custom Js -->
    <script src="<?php echo base_url();?>public/js/login.js"></script>
</body>

</html>