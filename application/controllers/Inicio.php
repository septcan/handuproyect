<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
    }
	public function index()
	{
    	$this->load->view('templates/header');
        $this->load->view('templates/navbar');
		$this->load->view('inicio');
		$this->load->view('templates/footer');
        $this->load->view('iniciojs');
	}
}
