<?php
class Login_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    function login($usuario){
        $strq = "SELECT usu.UsuarioID,per.personalId,per.nombre, usu.perfilId, usu.contrasena,c.idcliente
                FROM usuarios as usu 
                LEFT JOIN personal as per on per.personalId=usu.personalId
                LEFT JOIN cliente as c on c.idcliente=usu.idcliente
                where usu.Usuario ='".$usuario."'";
                log_message('error', 'sql: '.$strq);
        $query = $this->db->query($strq);
        return $query->result();
    }

    function getMenus($perfil)
    {
        $strq ="SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='$perfil'";
        $query = $this->db->query($strq);
        return $query->result();
    } 

    function submenus($perfil,$menu){
        $strq ="SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon from menu_sub as menus, perfiles_detalles as perfd WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='$perfil' and menus.MenuId='$menu' ORDER BY menus.orden ASC";
        $query = $this->db->query($strq);
        return $query->result();
    }
    function get_record($table,$col,$id){
        $sql = "SELECT * FROM $table WHERE $col=$id";
        $query = $this->db->query($sql);
        return $query->row();
    }

}